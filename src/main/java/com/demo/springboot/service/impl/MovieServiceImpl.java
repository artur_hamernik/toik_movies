package com.demo.springboot.service.impl;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.service.MovieService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {
    private MovieListDto movies;
    private int counter = 2;

    public MovieServiceImpl(){
        List<MovieDto> moviesList = new ArrayList<>();
        moviesList.add(new MovieDto(1,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );
        movies = new MovieListDto(moviesList);
    }

    @Override
    public MovieListDto getMovies() {
        List<MovieDto> movieSort = new ArrayList<>();
        for(int i = movies.getMovies().size()-1;  i >= 0; i--){
            movieSort.add(movies.getMovies().get(i));
        }
        return new MovieListDto(movieSort);
    }

    @Override
    public void setMovies(MovieListDto movies) {
        this.movies = movies;
    }

    @Override
    public void addMovie(CreateMovieDto movie) {
        movies.getMovies().add(new MovieDto(counter, movie.getTitle(), movie.getYear(),movie.getImage()));
        counter++;
    }

    @Override
    public boolean updateMovie(int id, CreateMovieDto createMovieDto) {
        for (MovieDto movie:movies.getMovies()
        ) {
            if(movie.getMovieId().equals(id)){
                if(createMovieDto.getTitle() != null){
                    movie.setTitle(createMovieDto.getTitle());
                }
                if(createMovieDto.getYear() != null){
                    movie.setYear(createMovieDto.getYear());
                }
                if(createMovieDto.getImage() != null){
                    movie.setImage(createMovieDto.getImage());
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean deleteMovie(int id) {
        for(int i= 0; i < movies.getMovies().size(); i++){
            if(movies.getMovies().get(i).getMovieId().equals(id)){
                movies.getMovies().remove(i);

                return true;
            }
        }
        return false;
    }


}
