package com.demo.springboot.service;


import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieListDto;

public interface MovieService {
    MovieListDto getMovies();
    void setMovies(MovieListDto movies);
    void addMovie(CreateMovieDto movie);
    boolean updateMovie(int id, CreateMovieDto createMovieDto);
    boolean deleteMovie(int id);
}
