package com.demo.springboot.rest;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.service.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController()
@RequestMapping("/api")
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    MovieService movieService;

    @GetMapping("/movies")//GET READ
    public ResponseEntity<MovieListDto> getMovies() {
        return ResponseEntity.ok().body(movieService.getMovies());    // = new ResponseEntity<>(movies, HttpStatus.OK);
    }

    @PutMapping("/movies/{id}")//PUT UPDATE
    public ResponseEntity<Void> updateMovie(@PathVariable("id") Integer id, @RequestBody CreateMovieDto createMovieDto) {
        if(movieService.updateMovie(id,createMovieDto)){
            return ResponseEntity.ok().build();
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/movies/{id}")//DELETE DELETE
    public ResponseEntity<Void> deleteMovie(@PathVariable("id") Integer id) {
        if(movieService.deleteMovie(id)){
            return ResponseEntity.ok().build();
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/movies") //POST CREATE
    public ResponseEntity<Void> createMovie(@RequestBody CreateMovieDto createMovieDto) throws URISyntaxException {
        if(createMovieDto.getTitle() == null || createMovieDto.getTitle().isEmpty() || createMovieDto.getImage() == null || createMovieDto.getYear() == null || createMovieDto.getImage().isEmpty()){
            return ResponseEntity.badRequest().build();
        }
        LOG.info("--- title: {}", createMovieDto.getTitle());
        movieService.addMovie(createMovieDto);
        return ResponseEntity.created(new URI("/movies/")).build();
    }
}
